import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
import numpy as np
import sys
import chardet
from sklearn.model_selection import KFold
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder
import pickle
from os.path import abspath
from nltk.corpus import stopwords
import re
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
from keras import backend as K
from keras.models import Sequential
from keras.layers import Dense, Embedding, LSTM, SpatialDropout1D
from keras.callbacks import EarlyStopping
from imblearn.over_sampling import SMOTE
from sklearn.utils.class_weight import compute_class_weight
from sklearn.metrics import classification_report


path = abspath('Tools.py')
size_path = len(path) - 9
path = path[0:size_path]
sys.path.append(path)
from Tools import *

"""
Función que permite verificar que los argumentos
introducidos en la terminal sean correctos para la
correcta ejecución del  programa
"""
def verify_args(argvs):
    if len(argvs)==6:
        if argvs[2].split('.')[1] == 'csv':
            filename = argvs[2]
        else:
            print("El archivo no está en formato .csv")
            exit()
        if argvs[4].split('.')[1] == 'h5':
            model_name = argvs[4]
        else:
            print("El archivo no tiene extensión .h5")
            exit()
        if argvs[5].split('.')[1] == 'pkl':
            tokenizer_name = argvs[5]
        else:
            print('El archivo no tiene extensión .pkl')
            exit()
    else:
        print("Error en los argumentos")
        exit()
    return filename, model_name, tokenizer_name

"""
Función para guardar los modelos del clasificador,
vectorizador y codificador de etiquetas
"""
def save_models(model_name, model):
    model_file = open(model_name,'wb')
    pickle.dump(model, model_file)

"""
Función para normalizar el texto, en caso de que
la oferta esté vacía, coloca un "-"
"""
def normalize_text(text):
    if isinstance(text,float):
        text = '-'
    else:
      text = text.strip()
    return text

REPLACE_BY_SPACE_RE = re.compile('[/(){}\[\]\|@,;]')
BAD_SYMBOLS_RE = re.compile('[^0-9a-záéíóú #+_]')
STOPWORDS = set(stopwords.words('spanish'))
MULTIPLE_WHITESPACES_RE = re.compile(' +')
"""
Función para realizar limpieza del texto, permite:
    1) Converir a minusculas el texto
    2) Eliminar simbolos del texto
    3) Eliminar palabras funcionales
"""
def clean_text(text):
    """
        text: a string
        
        return: modified initial string
    """
    text = text.lower() # lowercase text
    text = REPLACE_BY_SPACE_RE.sub(' ', text)
    text = BAD_SYMBOLS_RE.sub(' ', text)
    text = MULTIPLE_WHITESPACES_RE.sub(' ', text) 
    text = ' '.join(word for word in text.split() if word not in STOPWORDS)
    return text.strip()

"""
Metricas de evaluación para el modelo
"""
def precision(y_true, y_pred):	
    """Precision metric.	
     Only computes a batch-wise average of precision.	
     Computes the import tensorflow as tf
     precision, a metric for multi-label classification of	
    how many selected items are relevant.	
    """	
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))	
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))	
    precision = true_positives / (predicted_positives + K.epsilon())	
    return precision

def recall(y_true, y_pred):	
    """Recall metric.	
     Only computes a batch-wise average of recall.	
     Computes the recall, a metric for multi-label classification of	
    how many relevant items are selected.	
    """	
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))	
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))	
    recall = true_positives / (possible_positives + K.epsilon())	
    return recall

#######################################
#       EJECUCIÓN DEL PROGRAMA        #
#######################################
filename, model_name, tokenizer_name = verify_args(sys.argv)
data = get_data(filename)
# LIMPIEZA DEL TEXTO #
data['texto'] = data['texto'].apply(normalize_text)
data['texto'] = data['texto'].apply(clean_text)

# TOKENIZACIÓN #
# Número máximo de palabras que serán utilizadas (más frecuentes)
MAX_NB_WORDS = 50000
# Número máxmio de palabras en cada oferta de trabajo
MAX_SEQUENCE_LENGTH = 350
EMBEDDING_DIM = 300
tokenizer = Tokenizer(num_words=MAX_NB_WORDS, filters='!"#$%&()*+,-./:;<=>?@[\]^_`{|}~',lower=True)
tokenizer.fit_on_texts(data['texto'].values)
word_index = tokenizer.word_index

# Obtención de conjuntos X y Y para el clasificador #
x = tokenizer.texts_to_sequences(data['texto'].values)
x = pad_sequences(x, maxlen=MAX_SEQUENCE_LENGTH)
y = pd.get_dummies(data['area_trab']).values

# DIVISION DE CONJUNTOS DE ENTRENAMIENTO Y PRUEBA #
x_train, x_test, y_train, y_test = train_test_split(x,y, test_size=0.20, random_state=42)

# OVERSAMPLING UTILIZANDO SMOTE # 
smote = SMOTE('minority')
xSMOTE_train, ySMOTE_train = smote.fit_sample(x_train, y_train)

# USO DE BIAS EN CLASES MINORITARÍASfeatures, ngram, #
y_train_labels = [np.argmax(label) for label in y_train]
class_weights = compute_class_weight('balanced', np.unique(y_train_labels), y_train_labels)

# CREACIÓN DEL MODELO #
# Número máximo de palabras que serán utilizadas (más frecuentes)
MAX_NB_WORDS = 50000
# Número máxmio de palabras en cada oferta de trabajo
MAX_SEQUENCE_LENGTH = 350
EMBEDDING_DIM = 100

model = Sequential()
model.add(Embedding(MAX_NB_WORDS, EMBEDDING_DIM, input_length=x.shape[1]))
model.add(SpatialDropout1D(0.2))
model.add(LSTM(100, dropout=0.2, recurrent_dropout=0.2))
model.add(Dense(23, activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy', precision, recall])

# ENTRENAMIENTO DEL MODELO #
epochs = 10
batch_size = 64

history = model.fit(xSMOTE_train, ySMOTE_train, epochs=epochs, batch_size=batch_size, validation_split=0.1, class_weight=class_weights, callbacks=[EarlyStopping(monitor='val_loss', patience=3, min_delta=0.0001)])


# EVALUACIÓN DEL MODELO #
y_predicted = model.predict_classes(x_test)
y_true = []
for element in y_test:
  y_true.append(np.argmax(element))
y_true = np.array(y_true)
print(classification_report(y_true, y_predicted))


# GUARDADO DEL MODELO #
model.save(model_name)
with open(tokenizer_name, 'wb') as handle:
  pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)

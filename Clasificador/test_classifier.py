import pandas as pd
import sys
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.svm import SVC
from sklearn.preprocessing import LabelEncoder
from os.path import abspath
from keras import backend as K
import keras
from keras.preprocessing.sequence import pad_sequences
from nltk.corpus import stopwords
import re

path = abspath('Tools.py')
size_path = len(path) - 9
path = path[0:size_path]
sys.path.append(path)
from Tools import *

"""
Función que permite verificar que los argumentos
introducidos en la terminal sean correctos para la
correcta ejecución del  programa
"""
def verify_args(argvs):
    if len(argvs)==5:
        if argvs[2].split('.')[1] == 'csv':
            filename = argvs[2]
        else:
            print("El archivo no está en formato .csv")
            print(argvs[2].split('.'))
            exit()
        if argvs[3].split('.')[1] == 'h5' and argvs[4].split('.')[1] == 'pkl':
            model_name = argvs[3]
            vectorizer_name = argvs[4]
        else:
            print("Error, los archivos del modelo y vectorizador son incorrectos")
            exit()
    else:
        print("Error en los argumentos")
        exit()
    return filename, model_name, vectorizer_name

"""
Metricas de evaluación para el modelo
"""
def precision(y_true, y_pred):	
    """Precision metric.	
     Only computes a batch-wise average of precision.	
     Computes the import tensorflow as tf
     precision, a metric for multi-label classification of	
    how many selected items are relevant.	
    """	
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))	
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))	
    precision = true_positives / (predicted_positives + K.epsilon())	
    return precision

def recall(y_true, y_pred):	
    """Recall metric.	
     Only computes a batch-wise average of recall.	
     Computes the recall, a metric for multi-label classification of	
    how many relevant items are selected.	
    """	
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))	
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))	
    recall = true_positives / (possible_positives + K.epsilon())	
    return recall

"""
Función para normalizar el texto, en caso de que
la oferta esté vacía, coloca un "-"
"""
def normalize_text(text):
    if isinstance(text,float):
        text = '-'
    else:
      text = text.strip()
    return text

REPLACE_BY_SPACE_RE = re.compile('[/(){}\[\]\|@,;]')
BAD_SYMBOLS_RE = re.compile('[^0-9a-záéíóú #+_]')
STOPWORDS = set(stopwords.words('spanish'))
MULTIPLE_WHITESPACES_RE = re.compile(' +')
"""
Función para realizar limpieza del texto, permite:
    1) Converir a minusculas el texto
    2) Eliminar simbolos del texto
    3) Eliminar palabras funcionales
"""
def clean_text(text):
    """
        text: a string
        
        return: modified initial string
    """
    text = text.lower() # lowercase text
    text = REPLACE_BY_SPACE_RE.sub(' ', text)
    text = BAD_SYMBOLS_RE.sub(' ', text)
    text = MULTIPLE_WHITESPACES_RE.sub(' ', text) 
    text = ' '.join(word for word in text.split() if word not in STOPWORDS)
    return text.strip()

def get_labels(y_predicted):
    labels = []
    for element in y_predicted:
        if element == 0:
            labels.append('Administración')
        elif element == 1:
            labels.append('Call center')
        elif element == 2:
            labels.append('Comercio exterior')
        elif element == 3:
            labels.append('Comunicación')
        elif element == 4:
            labels.append('Construcción')
        elif element == 5:
            labels.append('Diseño')
        elif element == 6:
            labels.append('Educación')
        elif element == 7:
            labels.append('Finanzas')
        elif element == 8:
            labels.append('Gastronomía')
        elif element == 9:
            labels.append('Gerencia')
        elif element == 10:
            labels.append('Ingeniería')
        elif element == 11:
            labels.append('Legales')
        elif element == 12:
            labels.append('Logística')
        elif element == 13:
            labels.append('Mercadotecnía')
        elif element == 14:
            labels.append('Minería')
        elif element == 15:
            labels.append('Oficios')
        elif element == 16:
            labels.append('Producción')
        elif element == 17:
            labels.append('Recursos humanos')
        elif element == 18:
            labels.append('Salud')
        elif element == 19:
            labels.append('Secretaria')
        elif element == 20:
            labels.append('Seguros')
        elif element == 21:
            labels.append('Tecnología')
        elif element == 22:
            labels.append('Ventas')
    return labels

#######################################
#       EJECUCIÓN DEL PROGRAMA        #
#######################################
filename, model_name, vectorizer_name = verify_args(sys.argv)
data = get_data(filename)
# CARGA DEL MODELO # 
classifier = keras.models.load_model(model_name, custom_objects={"precision":precision, "recall":recall})

# CARGA DEL VECTORIZADOR #
tokenizer = load_model(vectorizer_name)

# LIMPIEZA DEL TEXTO #
MAX_SEQUENCE_LENGTH = 350
data['texto'] = data['texto'].apply(normalize_text)
data['texto'] = data['texto'].apply(clean_text)
x = tokenizer.texts_to_sequences(data['texto'].values)
x = pad_sequences(x, maxlen=MAX_SEQUENCE_LENGTH)
y = classifier.predict_classes(x)
y_labeled = get_labels(y)
data['area_trab'] = y_labeled
data.to_csv(filename,encoding='utf8',index=False)


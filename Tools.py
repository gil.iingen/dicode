import chardet
import pandas as pd
import pickle
from nltk.stem.snowball import SnowballStemmer

"""
Función para obtener la codificación del archivo .csv
desde donde se obtienen los textos para entrenamiento y
evaluación del sistema
"""
def get_encoding(filename):
    f = open(filename, 'rb').read()
    result = chardet.detect(f)
    encoding = result['encoding']
    return encoding

"""
Función para obtener un dataframe a parir del
archivo .csv de entrada
"""
def get_data(filename):
    encoding = get_encoding(filename)
    dataframe = pd.read_csv(filename, encoding=encoding)
    return dataframe

"""
Función que permite normalizar los textos
en caso de que estos estén vacios.

def normalize_text(texts):
    for index in range(len(texts)):
        if isinstance(texts[index],float):
            texts[index]='-'
    return texts
"""
"""
Función que permite cargar los modelos del clasificador
y del vectorizador obtenidos durante la etapa de entrenamiento
para realizar predicciones.
"""
def load_model(model_name):
    model_file = open(model_name, 'rb')
    model = pickle.load(model_file)
    return model

"""
Función  que permite pasar a minusculas una colección de textos
contenidos en una columna de un dataframe de pandas.
"""
def text2lowercase(text_list):
    size = len(text_list)
    for index in range(size):
        text_list[index] = text_list[index].lower()
    return text_list
"""
Permite realizar el proceso de stemming de un texto
utilizando nltk
"""
def stemming(text, stemmer):
    stem_text = ''
    tokens = text.split(' ')
    for token in tokens:
        stem_text += stemmer.stem(token) + ' '
    return stem_text.strip()

"""
Realiza el proceso de stemming de la colección de textos
a clasificar
"""
def stemmingTexts(text_list, stemmer):
    stem_texts = []
    for text in text_list:
        stem_texts.append(stemming(text,stemmer))
    return stem_texts


"""
Realiza la remoción de acentos de los textos
a clasificar
"""
def removeAccents(text):
    accents = ['á', 'é', 'í', 'ó', 'ú']
    no_accents = ['a', 'e', 'i', 'o', 'u']
    for index in range(len(accents)):
        text = text.replace(accents[index], no_accents[index])
    return text

def removeAccentsFromText(text_list):
    clean_text = []
    for text in text_list:
        clean_text.append(removeAccents(text))
    return clean_text


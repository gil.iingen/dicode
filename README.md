#  Diagnóstico de Competencias Demandadas (DiCoDe)

El sistema consta de los módulos siguientes:
* Clasificador de ofertas de trabajo.
* Detección de clases
* Identificación de términos

A continuación se señalan las dependencias necesarias para el funcionamiento del sistema, así como instrucciones para utilizar cada uno de los módulos.

## Dependencias necesarias
Las bibliotecas necesarias para el funcionamiento del sistema son:
* [Pandas](https://pandas.pydata.org/)
* [Sklearn](https://scikit-learn.org/stable/)
* [NumPy](https://numpy.org/)
* [chardet](https://chardet.github.io/)
* [NLTK](https://www.nltk.org/)
* [Keras](https://keras.io/)
* [Imbalanced-learn](https://imbalanced-learn.readthedocs.io/en/stable/api.html)

## Usos

### 1) Clasificador de ofertas de trabajo
#### Entrenamiento
```
python3 train_classifier.py -i input_data.csv -o classifier_name.h5 tokenizer_name.pickle
```
Donde:
##### Entrada:
* input_data.csv: archivo de entrada en formato .csv donde se encuentran los textos a clasificar y las etiquetas de cada uno de estos textos.
##### Salida:
* classifier_name: Indica el nombre del archivo donde se guardará el modelo del clasificador para utilizarse durante la etapa de predicción.
* tokenizer_name: Indica el nombre del archivo donde se guardará el modelo del tokenizador para utilizarse durante la etapa de predicción.

#### Predicción
```
python3 test_classifier.py -i data.csv classifier.h5 tokenizer.pickle
```
Donde:
##### Entrada:
* data.csv: Archivo que contiene el texto para realizar predicciones sobre él.
* classifier.h5: Archivo que contiene el modelo del clasificador obtenido durante el entrenamiento y que permitrá la predicción de los textos nuevos.
* tokenizer.pickle: Archivo que contiene el modelo del tokenizador obtenido durante el entrenamiento y que permitrá la predicción de los textos nuevos.
##### Salida:
* La salida del proceso de predicción será el mismo archivo data.csv, con la adición de la nueva columna donde se indica la clase de cada uno de los textos.


### 2) Detección de clases
Para utilizar este módulo es necesario utilizar 3 programas en el siguiente orden:
```
perl preproc.perl <input_data.csv >ANALYSE/output_data.csv.preproc
```
Donde:
* *input_data.csv:* Es el archivo donde se encuentran las ofertas de empleo de las cuales se quiere extraer sexo, salario, edad, horario, competencias, capacidades, experiencia, requerimientos y funciones.
* *output_data.csv.preproc:* Es el archivo donde se almacena el texto de cada oferta de trabajo después de realizar preprocesamiento para posteriormente realizar las búsquedas.

```
perl analyse.perl <PREPROC/input_data.csv.preproc > ANALYSE/output_data.analyse.csv
```
Donde:
* *input_data.csv:* Es el archivo preprocesado en el cual se buscarán los campos sexo, salario, edad y horario.
* *output_data.analyse.csv:* Es el archivo donde se guarda la información de los campos mencionados y el texto, cada uno separado por *\t*. 

```
python3 get_reqs.py
```
Donde una vez hecho el análisis del los archivos que contengan las ofertas de trabajo con el programa anterior, este buscará competencias, capacidades, experiencia, requerimientos y funciones de todos los archivos contenidos en la carpeta ANALYSE, y devolverá un archivo .csv que contiene los campos ID, SEXO, SALARIO, EDAD, HORARIO, HORARIO2, DEDICACIÓN, COMPETENCIAS, CAPACIDADES, EXPERIENCIAS,REQUISITOS, FUNCIONES, URL, TEXTO. Todos separados por "," y se almacenarán en la carpeta OUT.


**NOTA: Es importante mantener la organización de las carpetas** 

**NOTA 2: La carpeta CSVs deberá contener los archivos csv que contienen las ofertas de trabajo en las cuales se realizará la búsqueda de los campos descritos con anterioridad**

### 3) Identificación de términos
Este módulo obtiene todos los valores de los campos sexo, salario, edad, horario,competencias, capacidades, experiencia,requerimientos y funciones de una determinada área de trabajo.

Para ejecutar el programa es necesario utilizar el comando siguiente:

```
python3 get_requirementsForArea.py -i data.csv area
```

Donde:
* *data.csv:* Es el archivo donde se encuentran la categoría de las ofertas de empleo así como los campos sexo, salario, edad, horario,competencias, capacidades, experiencia,requerimientos y funciones.

* *area:* Es el área del cual se buscarán los campos mencionados.
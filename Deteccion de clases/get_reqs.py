import sys
import re
import pandas as pd
from os.path import abspath
import os
path = os.path.abspath('Tools.py')
sys.path.append(path)
from Tools import *

#############################################
# Funciones para búsqueda de requerimientos #
#############################################

def reduce_requirements(requirements_list):
    final_req_list = []
    for requirement in requirements_list:
        if requirement not in final_req_list:
            final_req_list.append(requirement)
    return final_req_list

def search_requirements(text, requirements):
    text = text.lower()
    text = removeAccents(text)
    req_list = list()
    for requirement in requirements:
        size = len(re.findall(r'\b{}\b'.format(requirement),text))
        if size > 0:
            req_list.append(requirement)
    reqs = ''
    for req in req_list:
        reqs += req + '.'
    reqs = reqs.strip()
    limit = len(reqs)-1
    return reqs[0:limit]

def get_requirements_lists():
    # Lectura del archivo .xlsx que contiene los requerimientos a buscar en el texto libre
    columns = ['COMPETENCIA/HABILIDAD', 'CAPACIDAD', 'EXPERIENCIA', 'REQUISITOS','FUNCIONES']
    data = pd.read_excel('Diccionarios.xlsx', sheet_name='Buena', usecols=columns)

    # Almacenamiento de los requerimientos en listas
    competencias = [competencia for competencia in data[columns[0]] if isinstance(competencia,str)]
    capacidades = [capacidad for capacidad in data[columns[1]] if isinstance(capacidad,str)]
    experiencias = [experiencia for experiencia in data[columns[2]] if isinstance(experiencia,str)]
    requisitos = [requisito for requisito in data[columns[3]] if isinstance(requisito,str)]
    funciones = [funcion for funcion in data[columns[4]] if isinstance(funcion,str)]

    # Pre-procesamiento de los requerimientos para reducir la variabilidad
    competencias = text2lowercase(competencias)
    capacidades = text2lowercase(capacidades)
    experiencias = text2lowercase(experiencias)
    requisitos = text2lowercase(requisitos)
    funciones = text2lowercase(funciones)

    competencias = removeAccentsFromText(competencias)
    capacidades = removeAccentsFromText(capacidades)
    experiencias = removeAccentsFromText(experiencias)
    requisitos = removeAccentsFromText(requisitos)
    funciones = removeAccentsFromText(funciones)

    competencias = reduce_requirements(competencias)
    capacidades = reduce_requirements(capacidades)
    experiencias = reduce_requirements(experiencias)
    requisitos = reduce_requirements(requisitos)
    funciones = reduce_requirements(funciones)
    return competencias, capacidades, experiencias, requisitos, funciones

##########################################################
# Funciones para obtener la URL de una oferta de trabajo #
##########################################################
def get_url_from_csv(file_path):
    df = pd.read_csv(file_path, encoding='utf8')
    urls = [url for url in df['url']]
    return urls

##################################################
# Funciones para lectura de archivos analyse.csv #
##################################################

def get_columns_values(analyse_file, column_index):
    analyse_file = analyse_file.split('\n')
    file_len = len(analyse_file) - 1
    analyse_file = analyse_file[2:file_len]
    values = []
    for row in analyse_file:
        data = [element for element in row.split('\t')]
        if(len(data)==8):
            values.append(data[column_index])
    return values

COMMA_RE = re.compile(',')    
def get_requirements_from_analyse_file(file_path, competencias=None, capacidades=None, experiencias=None, requisitos=None, funciones=None):
    analyse_file = open(file_path, encoding='utf8').read()
    # Get file columns
    columns = ['#ID','COMPETENCIAS','CAPACIDADES','EXPERIENCIA','REQUISITOS','FUNCIONES']
    aux_line = analyse_file.split('\n')[1].strip()
    # Obtención de la información contenida en los archivos contenidos en la carpeta ANALYSE
    # (0)ID, (1)SEXO, (2)SALARIO, (3)EDAD, (4)HORARIO, (5)HORARIO2, (6)DEDICACIÓN, (7)TEXTO
    id_list = get_columns_values(analyse_file,0)
    sexo_list = get_columns_values(analyse_file,1)
    salario_list = get_columns_values(analyse_file,2)
    edad_list = get_columns_values(analyse_file,3)
    horario_list = get_columns_values(analyse_file,4)
    horario2_list = get_columns_values(analyse_file,5)
    dedicacion_list = get_columns_values(analyse_file,6)
    texto_list = get_columns_values(analyse_file,7)

    #Listas para almacenar los requerimientos de cada oferta de trabajo
    comp_list = [search_requirements(text,competencias) for text in texto_list]
    cap_list = [search_requirements(text,capacidades) for text in texto_list]
    exp_list = [search_requirements(text,experiencias) for text in texto_list]
    req_list = [search_requirements(text,requisitos) for text in texto_list]
    func_list = [search_requirements(text,funciones) for text in texto_list]

    #Eliminación de commas
    texto_list = [COMMA_RE.sub('',text) for text in texto_list]
    # Obtención de URLs de cada oferta
    url_aux_file = file_path.split('ANALYSE/')[1].replace('.analyse','')
    urls_path = 'CSVs/{}'.format(url_aux_file)
    urls = get_url_from_csv(urls_path)

    #Escritura del nuevo archivo
    destination_filename = file_path.split('ANALYSE/')[1]
    destination_filename = destination_filename.replace('analyse','out')
    destination_file = open('OUT/{}'.format(destination_filename), 'w', encoding='utf8')
    column_line = 'ID,SEXO,SALARIO,EDAD,HORARIO,HORARIO2,DEDICACION,COMPETENCIAS,CAPACIDADES,EXPERIENCIAS,REQUISITOS,FUNCIONES,URL,TEXTO'
    destination_file.write('{}\n'.format(column_line.strip()))

    for index in range(len(id_list)):
        destination_file.write('{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n'.format(id_list[index],sexo_list[index],salario_list[index],edad_list[index],horario_list[index],horario2_list[index],dedicacion_list[index],comp_list[index],cap_list[index],exp_list[index],req_list[index],func_list[index],urls[index],texto_list[index]))
    destination_file.close()
    
#############
# Ejecución #
#############
competencias, capacidades, experiencias, requisitos, funciones = get_requirements_lists()

analyse_path = 'ANALYSE/'
for analyse_file in os.listdir(analyse_path):
    if os.path.isfile(os.path.join(analyse_path, analyse_file)):
        get_requirements_from_analyse_file('ANALYSE/{}'.format(analyse_file), competencias=competencias, capacidades=capacidades, experiencias=experiencias, requisitos=requisitos, funciones=funciones)